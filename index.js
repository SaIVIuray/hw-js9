function createListFromArray(arr, parent = document.body) {
    const ul = document.createElement("ul");

    arr.forEach(item => {
        const li = document.createElement("li");

        if (Array.isArray(item)) {
            createListFromArray(item, li);
        } else {
            li.textContent = item;
        }

        ul.appendChild(li);
    });

    parent.appendChild(ul);
}

const array1 = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
const array2 = ["1", "2", "3", "sea", "user", 23];
const array3 = ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];

createListFromArray(array1);
createListFromArray(array2);

const parentElement = document.getElementById("customList");
createListFromArray(array3, parentElement);

const countdownElement = document.createElement("div");
countdownElement.id = "countdown";
document.body.appendChild(countdownElement);

let countdown = 3;
const countdownInterval = setInterval(function () {
    countdownElement.textContent = countdown + " сек";
    countdown--;

    if (countdown < 0) {
        clearInterval(countdownInterval);
        document.body.innerHTML = "";
    }
}, 1000);
